// ------------------ Instructions information ------------------------ //

export const dataPipelineInstructionData = {
  id: 'PipeLineView',
  titleText: 'How to create a data processing pipeline:',
  paragraph: `First, select your data you want to process. Then select one or multiple data operations from the right.
  The result of a data pipeline is a dataset, which you can use directly to train a model or merge it into an existing branch.`
}

export const experimentInstructionData = {
  id: 'NewExperiment',
  titleText: 'How to create a new experiment:',
  paragraph: `First, select your data you want to do your experiment on. Then select one or multiple algorithms from the right.
      If needed, you can adapt the parameters of your algorithm directly`,
};

export const dataVisualizationInstuctionData = {
  id: 'EmptyDataVisualization',
  titleText: 'How to create a data visualization:',
  paragraph: `First, select your data you want to analyze. Then select one or multiple data visualizations from the right.
  After execution each visualization will be displayed in a new window.`,
}