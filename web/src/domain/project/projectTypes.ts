export enum PROJECT_TYPES {
  DATA_PROJ = 'data-project',
  CODE_PROJ = 'code-project'
}